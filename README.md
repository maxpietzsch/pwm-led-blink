# README #

This is a python script (primarily made for the Raspberry Pi), which uses WiringPi to create a PWM signal for a pulsating effect of a led.

You can call the script with the following parameters: 
```
pwm_led_blink.py <speed(lower value -> faster)> <wiringpi-Pin> <number of blinks>
```