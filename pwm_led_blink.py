# Pulsates an LED connected to GPIO pin 1 with a suitable resistor 4 times using softPwm
# softPwm uses a fixed frequency

###
# parameter: <pwm*.py> <SPEED(lower value -> faster)> <WIRINGPI-PIN> <NUMBERPULSES>
###
import wiringpi2
import sys


PIN_TO_PWM=0

LOOPDELAY=5

NUMBERPULSES=1

if len(sys.argv)==2:
	LOOPDELAY=int(sys.argv[1])

if len(sys.argv)==3:
	PIN_TO_PWM=int(sys.argv[2])
	LOOPDELAY=int(sys.argv[1])

if len(sys.argv)==4:
	NUMBERPULSES=int(sys.argv[3])
        LOOPDELAY=int(sys.argv[1])
	PIN_TO_PWM=int(sys.argv[2])



print NUMBERPULSES
print LOOPDELAY
print PIN_TO_PWM


OUTPUT = 1


wiringpi2.wiringPiSetup()
wiringpi2.pinMode(PIN_TO_PWM,OUTPUT)
wiringpi2.softPwmCreate(PIN_TO_PWM,0,100) # Setup PWM using Pin, Initial Value and Range parameters

for time in range(0,NUMBERPULSES):
	for brightness in range(0,100): # Going from 0 to 100 will give us full off to full on
		wiringpi2.softPwmWrite(PIN_TO_PWM,brightness) # Change PWM duty cycle
		wiringpi2.delay(LOOPDELAY) # Delay for 0.2 seconds
	for brightness in reversed(range(0,100)):
		wiringpi2.softPwmWrite(PIN_TO_PWM,brightness)
		wiringpi2.delay(LOOPDELAY)
